import * as config from "./config";
import texts from "../data.js";

const usernames=new Set;
const rooms = {};
/* {'room 1':{name:"room 1",users:
  'Oleg':{
    user:'Oleg',
    ready:false,
    progress:0
  }},
  finished:[]
} */


const joinRoom = (room,username) =>{
  rooms[room].users[username]= {
    user: username,
    ready:false,
    progress: 0
  }
}
const leaveRoom = (room,username) => {
  delete rooms[room].users[username];
  if ((Object.keys(rooms[room].users).length) === 0){
   delete rooms[room]
  }
}

const readyBtnTrue = (room,username) => {
  rooms[room].users[username].ready = true
}

const readyBtnFalse = (room,username) => {
  rooms[room].users[username].ready = false
}

const updateProgress = (room,username,progress) =>{
  rooms[room].users[username].progress = progress;
}

const playerFinished = (room,username) =>{
  rooms[room].finished.push(username)
}

const isAllPlayersFinised = (room) =>{
  return Object.keys(rooms[room].users).every( key =>rooms[room].users[key].progress === 100 )
}

const showWinnersTable = (room) =>{
  let results=rooms[room].finished;
  return results
}


const isAllPlayersReady = (room) =>{
  return Object.keys(rooms[room].users).every( key =>rooms[room].users[key].ready === true )
}

const ResetAll = (room) =>{
  Object.keys(rooms[room].users).map( key => {
    rooms[room].users[key].ready = false;
    rooms[room].users[key].progress = 0;
    rooms[room].finished=[];
  }  );
}
 

const isUserLogged = username => usernames.has(username);
const addUser = username => usernames.add(username);
const removeUser = username => usernames.delete(username);
const isUniqueRoomName = roomName => !(roomName in rooms);
let kick = false;


export default io => {
  io.on("connection", socket => {
    const username = socket.handshake.query.username;
    socket.emit("ISUSER_LOGGED",isUserLogged(username));

    if (!isUserLogged(username)){
      addUser(username)
    } else{
      kick= true
    }

    socket.emit('RENDER_LISTS_OF_ROOMS',rooms);
    socket.join('lobby');

    socket.on('CREATE', (roomName,roomObj) =>{
     const isUniq = isUniqueRoomName(roomName);

     if (isUniq){
      rooms[roomName]=roomObj
        socket.leave('lobby');
        socket.join(roomName);
        socket.emit("RENDER_GAME", rooms[roomName])
        io.in('lobby').emit("RENDER_LISTS_OF_ROOMS", rooms);
    } else {
      socket.emit('NOT_UNIQUE_ROOM_NAME');
    }
    })

    socket.on('JOIN_LOBBY',(roomName,username) =>{   
      socket.leave('lobby');
      joinRoom(roomName,username);
      socket.join(roomName);
      io.in(roomName).emit("RENDER_GAME", rooms[roomName]); 
    })

    socket.on('LEAVE_LOBBY',(roomName,username) => {
      leaveRoom(roomName,username);
      socket.leave(roomName);
      socket.join('lobby');
      if (rooms[roomName] === undefined){
        io.in('lobby').emit('RENDER_LISTS_OF_ROOMS',rooms);
      }
      io.in(roomName).emit("RENDER_GAME", rooms[roomName]); 
    })

    socket.on('READY_BUTTON',(roomName,username) =>{
      readyBtnTrue(roomName,username);
      if(isAllPlayersReady(roomName)){
    
        const randomNumber=Math.floor(Math.random() * Math.floor(7));
        io.in(roomName).emit("START",config.SECONDS_TIMER_BEFORE_START_GAME,randomNumber,config.SECONDS_FOR_GAME); 
      }
      io.in(roomName).emit("UPDATE_GAME", rooms[roomName].users); 
    })

    socket.on('NOTREADY_BUTTON',(roomName,username) =>{
      readyBtnFalse(roomName,username);
      io.in(roomName).emit("UPDATE_GAME", rooms[roomName].users); 
    })

    socket.on('UPDATEPROGRESS',(roomName,username,progress) =>{
      updateProgress(roomName,username,progress);
      io.in(roomName).emit("UPDATE_GAME", rooms[roomName].users);
      if (progress== 100){
        playerFinished(roomName,username);
          if (isAllPlayersFinised(roomName)){
            const res=showWinnersTable(roomName)
            io.in(roomName).emit("WINNER",res)
          }
      }

    })

    socket.on('TIME_LEFT',(roomName) =>{
     const res=showWinnersTable(roomName)
      io.in(roomName).emit("WINNER",res)
    })

    socket.on('RESET_ALL',(roomName)=>{
      ResetAll(roomName);
      io.in(roomName).emit("UPDATE_GAME", rooms[roomName].users)
    })

    
    socket.on("disconnect", () => {
      if(kick == false){
      removeUser(username);
      }else {
        kick=false;
      }
    });

  });
};


