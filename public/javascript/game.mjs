import { createElement, addClass, removeClass } from "./helper.mjs";

const username = sessionStorage.getItem("username");
let pointer = 0;
let roomNameG = '';
let textG = '';
let stopTimerG;

if (!username) {
  window.location.replace("/login");
}

const socket = io("", { query: { username } });

socket.on('ISUSER_LOGGED', (islogged) =>{
  if (islogged){
    sessionStorage.clear();
    window.location.replace("/login");
  }
})

const createButton = document.getElementById("create-button");

const onClickCreateButton = () =>{
  const roomName= prompt('Enter room name');
  const roomObj = {
    name: roomName,
    users: {
      [username]: {
      user: username,
      ready: false,
      progress: 0
      }
    },
    finished: [],
  }

 socket.emit('CREATE',roomName,roomObj);

}

createButton.addEventListener('click',onClickCreateButton);

const notUniqueRoomName = () =>{
  alert('Room with such name is already exists');
}

const renderLobbyRooms = (rooms) =>{
  const roomsKeys = Object.keys(rooms);
  const allRooms = roomsKeys.map( (key) =>{
    return renderLobbyRoom(rooms[key])
  })
  const container = document.getElementById('rooms-container');
  container.innerHTML='';
  container.append(...allRooms)
}

const renderLobbyRoom = (room) =>{

  const roomContainer = createElement({
    tagName: 'div',
    className: 'room',
    attributes: {id: `${room.name}`}
  });

  const connectedUsers = createElement({
    tagName: 'p',
    className: 'room-connected-users',
    attributes: {id: `${room.name}-connected`}
  })

  const roomName = createElement({
    tagName:'p',
    className: 'room-name',
    attributes: {id: `${room.name}-p`}
  })
  
  const roomButton = createElement({
    tagName: 'button',
    className: 'join-button',
    attributes: {id: `${room.name}-Btn`}
  })
  
  connectedUsers.innerText = `${Object.keys(room.users).length} user connected`
  roomName.innerText = `${room.name}`;
  roomButton.innerText = 'Join';
  roomButton.addEventListener('click',() => joinLobby(room.name,username));
  roomContainer.append(connectedUsers,roomName,roomButton);

  return roomContainer

}

const renderGame = (room) =>{
  roomNameG = room.name;

  const gamePage = document.getElementById('game-page');
  const roomPage = document.getElementById('rooms-page');
  roomPage.style.display = 'none';
  gamePage.style.display = 'block';
  gamePage.innerHTML='';

  const roomName = createElement({
    tagName: 'p',
    className: 'game-room-name',
  })
  
  const backBtn = createElement({
    tagName: 'button',
    className: '',
    attributes: {id: 'back-to-rooms-button'}
  })

  const userList = createElement({
    tagName: 'div',
    className: 'users-list',
    attributes: {id: 'users-container'}
  })

  const gameBox = createElement({
    tagName: 'div',
    className: '',
    attributes: {id:'game-box'}
  })

  const counterBeforeStart = createElement({
    tagName:'p',
    className: '',
    attributes: {id:'counterBeforeStart'}
  })

  const textForTyping  = createElement({
    tagName: 'p',
    className: '',
    attributes: {id:'textForTyping'}
  })

  const timeLeft = createElement({
    tagName:'span',
    className: '',
    attributes: {id: 'time-left'}
  })

  const buttonRdy = createElement({
    tagName: 'button',
    className: 'buttonRdy',
    attributes: {id: 'ReadyBtn'}
  })

  counterBeforeStart.style.display='none';
  textForTyping.style.display='none';
  timeLeft.style.display='none';
  roomName.innerText= `Room name: ${room.name}`;
  backBtn.innerText='Back to rooms';
  backBtn.addEventListener('click',() => backToLobby(room.name,username));

  buttonRdy.innerText='Ready';
  buttonRdy.addEventListener('click',() => btnReady(room.name,username));

  gameBox.append(buttonRdy,counterBeforeStart,textForTyping,timeLeft);

  gamePage.append(roomName,backBtn,userList,gameBox);

  updateGame(room.users);

}

const renderPlayer = playerObj =>{

  const wrapper = createElement({
    tagName: 'div',
    className: 'game-user-wrapper'
  })

  const isReady = playerObj.ready? 'ready' :'not-ready'
  
  const circle = createElement({
    tagName: 'div',
    className: `circle ${isReady}`,
    attributes: {id: `circle-${playerObj.user}`}
  })

  const usernamep = createElement({
    tagName: 'p',
    className: `game-username`
  })
  
  usernamep.innerText= username === playerObj.user ? `${username} (you)`: `${playerObj.user}`;

  const progressBar = createElement({
    tagName:'div',
    className: 'progressbar'
  })

  const spanProgres = createElement({
    tagName:'span',
    className: '',
    attributes: {id: `progress-${playerObj.user}`,
                style: `width:${playerObj.progress}%`}
  })

  if (playerObj.progress==100){
    spanProgres.style.backgroundColor = 'lime';
  }

  progressBar.append(spanProgres);

  wrapper.append(circle,usernamep,progressBar);

  return wrapper
}

const updateGame = (users) =>{
  const container=document.getElementById('users-container');

  container.innerHTML='';
  const UsersInRoom= Object.keys(users);
  const allUsers = UsersInRoom.map( (key)=>{
    return renderPlayer(users[key]);
  })

  container.append(...allUsers);

}

const backToLobby= (roomName,username) =>{
  const gamePage = document.getElementById('game-page');
  const roomPage = document.getElementById('rooms-page');
  roomPage.style.display = 'block';
  gamePage.style.display = 'none';
  roomNameG='';
  socket.emit('LEAVE_LOBBY',roomName,username);
}

const joinLobby= (roomName,username) =>{

  socket.emit('JOIN_LOBBY',roomName,username);
}

const btnReady = (roomname,username) =>{
  const btn=document.getElementById('ReadyBtn');
    if (btn.innerHTML==='Ready'){
      socket.emit('READY_BUTTON',roomname,username);
      btn.innerHTML='Not Ready';
    } else {
      socket.emit('NOTREADY_BUTTON',roomname,username);
      btn.innerHTML='Ready';
    }
}

const startGame = (delay,randomText,secondsForGame) =>{
  document.getElementById('ReadyBtn').style.display= 'none';
  document.getElementById('back-to-rooms-button').style.display= 'none';
  const counterElem = document.getElementById('counterBeforeStart');
  const textElement = document.getElementById('textForTyping');
  counterElem.style.display = 'block';

  fetch(`http://localhost:3002/game/texts/${randomText}`)
    .then(data=> data.json())
    .then(res => textElement.innerHTML= res)

   const id=setInterval(()=>{
    if(delay==0){
      clearInterval(id);
      counterElem.style.display = 'none';
      textElement.style.display= 'block';
      textG= document.getElementById('textForTyping').innerText;
      modifyText();
      document.addEventListener('keypress',gameCallback);
      startTimerForGame(secondsForGame);
    }
    counterElem.innerText=`${delay}`;
    delay--
  },1000)
}

const gameCallback = (event) =>{
  
  
 if(textG.charCodeAt(pointer) == event.keyCode){
   pointer++
  let progress= pointer/textG.length *100;
  modifyText();

  socket.emit('UPDATEPROGRESS',roomNameG,username,progress);
 }
}

const modifyText = () =>{
  let str1 = textG.slice(0,pointer);
  let str2 = textG.slice(pointer,pointer+1);
  let str3 = textG.slice(pointer+1);
  let newStr = `<span style='background-color:green'>${str1}</span><span style='text-decoration:underline'>${str2}</span>${str3}`;
  document.getElementById('textForTyping').innerHTML=newStr;
  
}

const startTimerForGame= (secondsForGame) =>{ 
  const timeLeftElem = document.getElementById('time-left');
  timeLeftElem.style.display= 'block';
  timeLeftElem.innerText=`${secondsForGame} seconds left`;

   stopTimerG=setInterval(()=>{
    if(secondsForGame==0){
      clearInterval(stopTimerG);
      socket.emit('TIME_LEFT',roomNameG);
    }
    timeLeftElem.innerText=`${secondsForGame} seconds left`;
    secondsForGame--
  },1000)
}

 const showWinner= (finished) =>{
   afterGame();
   const finishedPlayers= finished.length;
   let res='';
   let pointer=0;
  while(finishedPlayers!==pointer){
    res+= `#${pointer+1} ${finished[pointer]}`;
    pointer++
  }
  if(finishedPlayers===0){
    alert('No one finished')
  }else {
    alert(res);
  }
  afterResults(); 
} 

const afterGame = () =>{
  socket.emit('RESET_ALL',roomNameG);
  document.getElementById('textForTyping').style.display='none';
  document.getElementById('textForTyping').innerHTML='';
  document.getElementById('time-left').style.display= 'none';
  document.getElementById('time-left').innerHTML='';
  document.getElementById('counterBeforeStart').innerText= '';
  document.removeEventListener('keypress',gameCallback);
  pointer= 0;
  clearInterval(stopTimerG);
}

const afterResults= () =>{
  document.getElementById('ReadyBtn').innerHTML='Ready';
  document.getElementById('ReadyBtn').style.display= 'block';
  document.getElementById('back-to-rooms-button').style.display= 'block';
}
    

socket.on('NOT_UNIQUE_ROOM_NAME', notUniqueRoomName);
socket.on("RENDER_LISTS_OF_ROOMS", renderLobbyRooms );
socket.on("RENDER_GAME", renderGame);
socket.on("UPDATE_GAME", updateGame);
socket.on("START",startGame);
socket.on("WINNER",showWinner); 

